provider "aws" {
  version = "~> 2.33"
  region  = "us-east-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

variable "backend_servers" {
  default = "www.porterco.org:80"
}

variable "ssh_key" {
  default = ""
}

variable "admin_password" {
  default = ""
}
variable "smtp_password" {
  default = ""
}
variable "postgres_password" {
  default = ""
}
variable "ldap_password" {
  default = ""
}
variable "admin_email" {
  default = ""
}

variable "org_admin_password" {
  default = ""
}
variable "apg_repo_password" {
  default = ""
}
variable "apg_repo_user" {
  default = ""
}
variable "smtp_user" {
  default = ""
}
variable "smtp_host" {
  default = ""
}

data "template_file" "init" {
  template = "${file("${path.module}/startup.tpl")}"
  vars = {
    admin_password = "${var.admin_password}"
    smtp_host = "${var.smtp_host}"
    smtp_user = "${var.smtp_user}"
    smtp_password = var.smtp_password
    postgres_password = "${var.postgres_password}"
    ldap_password = "${var.ldap_password}"
    admin_email = "${var.admin_email}"
    org_admin_password = "${var.org_admin_password}"
    apg_license = "${data.local_file.license.content}"
    apg_repo_password = "${var.apg_repo_password}"
    apg_repo_user = "${var.apg_repo_user}"
  }
}

data "local_file" "license" {
  filename = "${path.module}/license"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "apg-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1b", "us-east-1a", "us-east-1c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true

  //  enable_vpn_gateway = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow http inbound traffic"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http_9000" {
  name = "allow_http_9000"
  description = "Allow http inbound traffic"
  vpc_id = "${module.vpc.vpc_id}"

  ingress {
    from_port = 9000
    to_port = 9000
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow ssh inbound traffic"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "jump_host" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.allow_ssh.id}"]
  subnet_id              = "${module.vpc.public_subnets[0]}"
  key_name               = "${var.ssh_key}"

  tags = {
    Name = "jump-host"
  }
}

resource "aws_launch_configuration" "as_conf" {
  image_id        = "ami-02eac2c0129f6376b"
  instance_type   = "t2.2xlarge"
  key_name        = "${var.ssh_key}"
  security_groups = ["${aws_security_group.allow_ssh.id}", "${aws_security_group.allow_http_9000.id}"]
  user_data       = "${data.template_file.init.rendered}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "apg_asg" {
  name                      = "terraform-apg-asg"
  launch_configuration      = "${aws_launch_configuration.as_conf.name}"
  max_size                  = 3
  min_size                  = 1
  desired_capacity          = 1
  health_check_grace_period = 400
  vpc_zone_identifier       = "${module.vpc.private_subnets}"
  health_check_type         = "EC2"
//  load_balancers            = ["apg-terraform-elb"]
  default_cooldown          = 400

  tag {
    key                 = "name"
    propagate_at_launch = true
    value               = "apg-asg"
  }

  depends_on = [aws_elb.apg_elb]
}

data "aws_instances" "apg_nodes" {
  instance_tags = {
    name = "apg-asg"
  }

  instance_state_names = ["running"]
  depends_on           = [aws_autoscaling_group.apg_asg]

}

resource "aws_elb_attachment" "attachment" {
  elb      = "${aws_elb.apg_elb.id}"
  instance = "${data.aws_instances.apg_nodes.ids[0]}"
  //it's okay to add just one instance, the ASG will automatically add the nodes that it creates to the ELB

  //  count = length(data.aws_instances.test.ids)
  //  instance = "${data.aws_instances.test.ids[count.index]}"
}

resource "aws_elb" "apg_elb" {
  name            = "apg-terraform-elb"
  security_groups = ["${aws_security_group.allow_http.id}"]
  subnets         = "${module.vpc.public_subnets}"

  //  access_logs {
  //    bucket        = "foo"
  //    bucket_prefix = "bar"
  //    interval      = 60
  //  }

  listener {
    instance_port     = 9000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:9000/login"
    interval            = 6
  }

  //  instances                   = "${data.aws_instances.test.ids}"
  cross_zone_load_balancing   = true
  idle_timeout                = 120
  connection_draining         = true
  connection_draining_timeout = 120

  tags = {
    Name = "apg-terraform-elb"
  }
}

output "elb_ip" {
  value = "http://${aws_elb.apg_elb.dns_name}"
}