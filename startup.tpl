#!/bin/bash
echo "IP1=REPLACE_ME
HOSTIP=\"\$IP1\"
ENABLE_SYSTEM_CHECK=y
ADMIN_EMAIL=${admin_email}
APIGEE_ADMINPW=${admin_password}
LICENSE_FILE=/tmp/license
MSIP=\$IP1
LDAP_TYPE=1
APIGEE_LDAPPW=${ldap_password}
MP_POD=gateway
REGION=dc-1
ZK_HOSTS=\"\$IP1\"
ZK_CLIENT_HOSTS=\"\$IP1\"
CASS_HOSTS=\"\$IP1\"
PG_PWD=${postgres_password}
SKIP_SMTP=n
SMTPHOST=${smtp_host}
SMTPUSER=${smtp_user}
SMTPPASSWORD=${smtp_password}
SMTPSSL=y
SMTPPORT=587
SMTPMAILFROM=\"Apigee MW <apigee-mw@mavenwave.com>\" " > /tmp/install-config

echo "IP1=REPLACE_ME
MSIP=\$IP1
ADMIN_EMAIL=${admin_email}
APIGEE_ADMINPW=${admin_password}
ORG_NAME=mw
NEW_USER=\"y\"
USER_NAME=new@user.com
FIRST_NAME=new
LAST_NAME=user
USER_PWD=${org_admin_password}
ORG_ADMIN=new@user.com
ENV_NAME=dev
VHOST_PORT=9001
VHOST_NAME=default
VHOST_ALIAS=mw-test.apigee.net" > /tmp/org-config

#set -e
setenforce 0
yum install wget -y
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
#rpm -ivh epel-release-latest-7.noarch.rpm
yum install libdb4 yum-plugin-priorities yum-utils java -y
curl https://software.apigee.com/bootstrap_4.18.05.sh -o /tmp/bootstrap_4.18.05.sh
bash /tmp/bootstrap_4.18.05.sh apigeeuser=${apg_repo_user} apigeepassword=${apg_repo_password}
/opt/apigee/apigee-service/bin/apigee-service apigee-setup install
IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)
sed 's/REPLACE_ME/'$IP'/g' /tmp/install-config > /usr/local/myConfig
sed 's/REPLACE_ME/'$IP'/g' /tmp/org-config > /usr/local/config-org
echo "${apg_license}" > /tmp/license
/opt/apigee/apigee-setup/bin/setup.sh -p aio -f /usr/local/myConfig
/opt/apigee/apigee-service/bin/apigee-service apigee-validate install
#/opt/apigee/apigee-service/bin/apigee-service apigee-validate setup -f /usr/local/myConfig
#/opt/apigee/apigee-service/bin/apigee-service apigee-validate clean  -f /usr/local/myConfig
/opt/apigee/apigee-service/bin/apigee-service edge-ui restart
/opt/apigee/apigee-service/bin/apigee-all status
/opt/apigee/apigee-service/bin/apigee-service apigee-provision install
/opt/apigee/apigee-service/bin/apigee-service apigee-provision setup-org -f /usr/local/config-org