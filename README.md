# Terraform Recipe to Install Apigee Edge

Module does the following:

- Creates a new VPC with private and public subnets.
- Creates a jump host in the public subnet
- Create two nodes in private subnet and installs nginx on them. Nginx reverse proxies to some other site.
- Create an ELB in the public subnet to load balance between the two nodes.

## How to run

- Clone this repo.
- Create your ssh key in AWS if you have not done so.
- Generate your AWS Creds and export them as env variables:
- `export AWS_SECRET_ACCESS_KEY=something`
- `export AWS_ACCESS_KEY_ID=another`
- Create `terraform.tfvars` file to initialize all variables, example:
```
admin_email = "aman.wolde@mavenwave.com"
admin_password = "blah"
apg_repo_user = "blah"
apg_repo_password = "blah"
smtp_host = "blah"
smtp_user = "blah"
smtp_password = "blah"
postgres_password = "blah"
ldap_password = "blah"
org_admin_password = "blah"
ssh_key = "awolde"
```
- `terraform init`
- `terraform apply -auto-approve`


## Cleaning
- `terraform destroy -auto-approve`

## TODO
- WIP to make LB work to Apigee Edge node
